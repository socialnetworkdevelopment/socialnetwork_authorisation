﻿using System.Collections.Generic;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;

namespace Infrastructure.IdentityServer
{
    public static class IdentityServerConiguration
    {

        /// <summary>
        /// Сервисы к которым identity server может дать доступ
        /// </summary>
        public static IEnumerable<ApiScope> ApiScopes =>
            new List<ApiScope>
            {
                new ApiScope("WebApplication", "test Api"),
            };
        public static IEnumerable<Client> Clients(string clientUrl) =>
            new List<Client>
            {
                new Client
                {
                    // unique ID for this client
                    ClientId = "react_UI", 
                    // human-friendly name displayed in IS
                    ClientName = "ReactApp", 
                    // URL of client
                    ClientUri = clientUrl,
                    // how client will interact with our identity server (Implicit is basic flow for web apps)
                    AllowedGrantTypes = GrantTypes.Code,
                    //AllowedGrantTypes = GrantTypes.Implicit, 
                    // don't require client to send secret to token endpoint
                    RequireClientSecret = false,
                    RedirectUris =
                    {             
                        // can redirect here after login
                        clientUrl+"/callback",
                        //"http://localhost:3000/signin-oidc",
                        },
                    // can redirect here after logout
                    PostLogoutRedirectUris =
                    {
                        clientUrl+"/login",
                    }, 
                    // builds CORS policy for javascript clients
                    AllowedCorsOrigins = { clientUrl }, 
                    // what resources this client can access
                    AllowedScopes = 
                    {
                         IdentityServerConstants.StandardScopes.OpenId, //openid
                         IdentityServerConstants.StandardScopes.Profile, //profile
                         IdentityServerConstants.StandardScopes.OfflineAccess //offline_access
                    }, 
                    // client is allowed to receive tokens via browser
                    AllowAccessTokensViaBrowser = true, 
                           
                    AllowOfflineAccess= true,


                    AccessTokenLifetime = 3600, //время действования токена доступа в секундах
                }
            };
        public static IEnumerable<IdentityResource> IdentityResources =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };

        public static IEnumerable<ApiResource> GetApis=>
            new ApiResource[]
            {
            };
    }
}
