using Domain.Entities;
using IdentityServer4;
using Infrastructure.EntityFramework;
using Infrastructure.IdentityServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

namespace SocialNetwork_Authorisation
{
    public class Program
    {
        public static void Main(string[] args)
        {
            AppSettings appSettings = new AppSettingsBuilder();

            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            builder.Services.AddCors(option =>
            {
                option.AddPolicy("auth_cors_politicy", app =>
                {
                    app.WithOrigins(appSettings.ReactClientUrl, "https://localhost:50005");
                    app.AllowAnyMethod();
                    app.AllowAnyHeader();
                    app.AllowCredentials();
                });
            });

            builder.Services.AddDbContext<DatabaseContext>(options =>
            {
                options.UseNpgsql(appSettings.ConnectionString);
            });

            builder.Services.AddTransient<AppSettings, AppSettingsBuilder>();

            #region IdentityServer



            builder.Services.AddIdentity<User, IdentityRole<long>>(c => {
                c.Password.RequireUppercase = false;
                c.Password.RequireDigit = false;
                c.Password.RequireNonAlphanumeric = false;
                c.Password.RequireLowercase = false;
            })
                .AddEntityFrameworkStores<DatabaseContext>();

            builder.Services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddInMemoryIdentityResources(IdentityServerConiguration.IdentityResources)
                .AddInMemoryApiScopes(IdentityServerConiguration.ApiScopes)
                .AddInMemoryClients(IdentityServerConiguration.Clients(appSettings.ReactClientUrl))
                .AddInMemoryApiResources(IdentityServerConiguration.GetApis);

            builder.Services.AddAuthentication()
                .AddOpenIdConnect("oidc", "IdentityServer", options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                    options.SignOutScheme = IdentityServerConstants.SignoutScheme;
                    options.SaveTokens = true;

                    options.Authority = appSettings.IdentityServerSettings?.Authority;
                    options.ClientId = "interactive.confidential";
                    options.ClientSecret = "secret";
                    options.ResponseType = "code";

                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = "name",
                        RoleClaimType = "role"
                    };
                });


            builder.Services.ConfigureApplicationCookie(config =>
            {
                config.Cookie.SameSite = SameSiteMode.None; //None ��� ���������� ����� ����� (http://) ��������,
                                                            //���� ��������� Lax � ��������� ������ � ���������� (https://) ��������
                config.Cookie.Name = appSettings.IdentityServerSettings?.Cookie?.Name;
                config.LoginPath = appSettings.IdentityServerSettings?.LoginPath;
                config.ReturnUrlParameter = "my_ReturnUrlParameter";
                config.LogoutPath = appSettings.IdentityServerSettings?.LogoutPath;
                config.ExpireTimeSpan = new System.TimeSpan(appSettings.IdentityServerSettings.Cookie.CookieDayLife, 0, 0, 0);
            });


            #endregion

            // Specifying the configuration for serilog
            SocialNetwork.Common.Logging.LogConfigurator.ConfigureSerilogToSeq(builder.Host, builder.Configuration);

            var app = builder.Build();


            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.MapControllers();

            app.UseCors("auth_cors_politicy");

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseIdentityServer();

            app.Run();
        }
    }
}
