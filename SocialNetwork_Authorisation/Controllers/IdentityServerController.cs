using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Domain.Entities;
using System.Linq;
using IdentityServer4;
using Infrastructure.EntityFramework;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Cors;
using System.Net;
using System.Text.Json;
using System.Text;
using System.Net.Http;

namespace SocialNetwork_Authorisation.Controllers
{
    [ApiController]
    [Route("auth")]
    [EnableCors]
    public class IdentityServerController : ControllerBase
    {

        private readonly ILogger<IdentityServerController> _logger;
        private readonly UserManager<User> _userManager;
        private readonly DatabaseContext _db;
        private readonly AppSettings _appSettings;

        public IdentityServerController(ILogger<IdentityServerController> logger, UserManager<User> userManager,
            DatabaseContext db, AppSettings appSettings)
        {
            _logger = logger;
            _userManager = userManager;
            _db = db;
            _appSettings = appSettings;
        }

        /// <summary>
        /// Try to login user
        /// </summary>
        /// <param name="loginName">login or email</param>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized, Type = typeof(string))]

        [Route("login")]
        public async Task<IActionResult> Login(
            [Required(AllowEmptyStrings = false,ErrorMessage = "loginName must not be null or empty")] string loginName,
            [Required(AllowEmptyStrings = false,ErrorMessage = "password must not be null or empty")] string password)
        {
            var user = await _userManager.FindByNameAsync(loginName);
            if (user == null) user = await _userManager.FindByEmailAsync(loginName);

            if (user == null) return StatusCode(404, "User is not found");

            if (user.PasswordHash == password.GetHash())
            {
                var props = new AuthenticationProperties
                {
                    IsPersistent = true,
                };

                var isuser = new IdentityServerUser(user.Id.ToString())
                {
                    DisplayName = user.Id.ToString(),
                };

                await HttpContext.SignInAsync(isuser, props);
                return StatusCode(200);
            }
            return StatusCode(401, "�� ������ ������ ");
        }

        /// <summary>
        /// Logout user
        /// </summary>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Route("logout")]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            Response.Cookies.Delete(_appSettings.IdentityServerSettings.Cookie.Name);
            return Redirect(_appSettings.ReactClientUrl+"/login");
        }

        /// <summary>
        /// Creating a user entity 
        /// </summary>
        /// <returns>Returns status code 200 if user success created, else code 400 and text error</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(string))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
        [Route("createUser")]
        public async Task<IActionResult> CreateUser(
            [Required(AllowEmptyStrings = false, ErrorMessage = "login must not be null or empty")] string login,
            [Required(AllowEmptyStrings = false, ErrorMessage = "pass must not be null or empty")] string pass,
            [Required(AllowEmptyStrings = false, ErrorMessage = "email must not be null or empty")] string email
            )
        {
            var oldUser = await _userManager.FindByEmailAsync(email);
            if (oldUser != null) return StatusCode(400, "������������ � ����� Email ��� ����������.");
            oldUser = await _userManager.FindByNameAsync(login);
            if (oldUser != null) return StatusCode(400, "������������ � ����� ������ ��� ����������.");

            var newUser = new User()
            {
                UserName = login,
                PasswordHash = pass.GetHash(),
                Email = email
            };
            var result = await _userManager.CreateAsync(newUser);

            if (!result.Succeeded)
                return StatusCode(400, result.Errors.ToList().FirstOrDefault());

            var createdUser = await _userManager.FindByNameAsync(login);

            try
            {
                var json = JsonSerializer.Serialize(new CreatedUserDto()
                {
                    Id = createdUser.Id,
                    FirstName = createdUser.UserName,
                    LastName = ""
                });

                using (var client = new HttpClient())
                {
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(_appSettings.UsersServiceUrl + "/api/users", content);
                   
                    if (response.StatusCode != HttpStatusCode.Created)
                    {
                        _userManager?.DeleteAsync(createdUser);
                        return StatusCode(400, "�� ������� ������� ������������ � ������� �������������");
                    }
                }
            }
            catch (Exception)
            {
                _userManager?.DeleteAsync(createdUser);
                return StatusCode(500, "������ ��� ��������� � ������ Users");
            }

            return StatusCode(201, createdUser.Id);
        }

        [HttpGet]
        [Route("LoginPage")]
        public IActionResult LoginPage() => Unauthorized();
    }
}