﻿namespace SocialNetwork_Authorisation.Controllers
{
    public class CreatedUserDto
    {
        public long Id { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public int? Age { get; set; }
    }
}
