﻿using System.Security.Cryptography;
using System;

namespace SocialNetwork_Authorisation.Controllers
{
    public static class Hash
    {
        public static string GetHash(this string s) 
        {
            var saltBytes = Convert.FromBase64String("dc6817ae74f74a61b55f09115a37d9935cbe0c32ed7c40d18c0732890fb0df13");
            var iterations = 50;
            using (var rfc2898DeriveBytes = new Rfc2898DeriveBytes(s, saltBytes, iterations))
            {
                return Convert.ToBase64String(rfc2898DeriveBytes.GetBytes(iterations));
            }
        }
    }
}
