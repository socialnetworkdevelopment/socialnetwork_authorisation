﻿using Microsoft.Extensions.Configuration;
using System;

namespace SocialNetwork_Authorisation
{
    public class AppSettingsBuilder : AppSettings
    {
        public AppSettingsBuilder()
        {
            var settings = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build()
                .Get<AppSettings>();

            base.ConnectionString = settings?.ConnectionString;
            base.ReactClientUrl = settings?.ReactClientUrl;
            base.IdentityServerSettings = settings?.IdentityServerSettings;
            base.RabbitMQSettings = settings?.RabbitMQSettings;
            base.ServiceSettings = settings?.ServiceSettings;
            base.UsersServiceUrl = settings?.UsersServiceUrl;
        }
    }

    public class AppSettings
    {
        public string? ConnectionString { get; set; }

        /// <summary>
        /// Url client for enabel CORS requests
        /// </summary>
        public string? ReactClientUrl { get; set; }
        public string? UsersServiceUrl { get; set; }
        public IdentityServerSettings? IdentityServerSettings { get; set; }
        public ServiceSettings? ServiceSettings { get; set; }
        public RabbitMQSettings? RabbitMQSettings { get; set; }

    }

    public class IdentityServerSettings
    {
        /// <summary>
        /// Url identity server
        /// </summary>
        public string? Authority { get; set; }
        /// <summary>
        /// Redirect url login page
        /// </summary>
        public string? LoginPath { get; set; }

        public string? LogoutPath { get; set; }
        public CookieSettings? Cookie { get; set; }
    }

    public class CookieSettings
    {
        /// <summary>
        /// Name file
        /// </summary>
        public string? Name { get; set; }
        /// <summary>
        /// Time life cooke
        /// </summary>
        public int CookieDayLife { get; set; }
    }

    public class ServiceSettings
    {
        public string? ServiceName { get; set; }
    }

    public class RabbitMQSettings
    {
        public string? Host { get; set; }
        public string? VirtualHost { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public int Port { get; set; }
    }
}
