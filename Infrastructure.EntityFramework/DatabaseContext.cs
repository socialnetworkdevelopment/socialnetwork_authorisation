﻿using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Reflection.Emit;


namespace Infrastructure.EntityFramework
{
    public class DatabaseContext : IdentityDbContext<IdentityUser<long>, IdentityRole<long>, long>
    {
        public DbSet<User> Users { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            this.SeedUsers(builder);
        }

        private void SeedUsers(ModelBuilder builder)
        {
            builder.Entity<User>().HasData(
                new User
                {
                    Id = 1,
                    UserName = "1",
                    NormalizedUserName= "1",
                    Email = "John@mail.ru",
                    NormalizedEmail = "John@mail.ru",
                    PasswordHash = "BlpN+zjFSz8Y5o/gp+TWTuoH70k13X/1tTX0lmUm+6jIUhFhpt4jkcwxHLQ0kao6T1s="
                },
                new User
                {
                    Id = 2,
                    UserName = "2",
                    NormalizedUserName= "2",
                    Email = "Miles@mail.ru",
                    NormalizedEmail = "Miles@mail.ru",
                    PasswordHash = "BlpN+zjFSz8Y5o/gp+TWTuoH70k13X/1tTX0lmUm+6jIUhFhpt4jkcwxHLQ0kao6T1s="
                },
                new User
                {
                    Id = 3,
                    UserName = "3",
                    NormalizedUserName= "3",
                    Email = "Petr@mail.ru",
                    NormalizedEmail = "Petr@mail.ru",
                    PasswordHash = "BlpN+zjFSz8Y5o/gp+TWTuoH70k13X/1tTX0lmUm+6jIUhFhpt4jkcwxHLQ0kao6T1s="
                }
            );
        }
    }
}