#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS base
WORKDIR /app

EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /src
COPY ["SocialNetwork_Authorisation/SocialNetwork_Authorisation.csproj", "SocialNetwork_Authorisation/"]
COPY ["Infrastructure.EntityFramework/Infrastructure.EntityFramework.csproj", "Infrastructure.EntityFramework/"]
COPY ["Domain.Entities/Domain.Entities.csproj", "Domain.Entities/"]
COPY ["Infrastructure.IdentityServer/Infrastructure.IdentityServer.csproj", "Infrastructure.IdentityServer/"]

COPY nuget.config /

# WORKDIR "C:/learn/SocialNetwork/socialnetworkbackend/packages/"
COPY SocialNetwork.Common.1.0.2.nupkg /src

WORKDIR /src

RUN dotnet restore "SocialNetwork_Authorisation/SocialNetwork_Authorisation.csproj" --configfile "/nuget.config"



COPY . .
WORKDIR "/src/SocialNetwork_Authorisation"
RUN dotnet build "SocialNetwork_Authorisation.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "SocialNetwork_Authorisation.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "SocialNetwork_Authorisation.dll"]