﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Domain.Entities
{
    /// <summary>
    /// Entity class
    /// </summary>
    public class User: IdentityUser<long>
    {
    }
}
